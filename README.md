# saas-b2b-boilerplate

## Boilerplate and Starter for Next JS 14+, Tailwind CSS 3.4, and TypeScript


🚀 Boilerplate and Starter for Next.js with App Router support, Tailwind CSS and TypeScript 
⚡️ Made with developer experience first: Next.js, TypeScript, ESLint, Prettier, Husky, VSCode, Tailwind CSS, Authentication with Clerk, Error Monitoring with Sentry, Multi-language (i18n), and more.