import { DashboardConfig } from "types"

export const dashboardConfig: DashboardConfig = {
  mainNav: [
   /*  {
      title: "Documentation",
      href: "/docs",
    },
    {
      title: "Support",
      href: "/support",
      disabled: true,
    }, */
  ],
  sidebarNav: [
    {
      title: "All users",
      href: "/dashboard",
      icon: "user",
    },
    {
      title: "Admin users",
      href: "/dashboard/admin",
      icon: "user",
    },
    {
      title: "Members",
      href: "/dashboard/members",
      icon: "user",
    },
  ],
}
