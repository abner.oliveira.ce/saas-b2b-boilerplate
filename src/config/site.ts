export const siteConfig = {
  name: "SaaS B2B Boilerplate",
  description:
    "An awesome SaaS B2B Boilerplate with Next.js, TypeScript, and Tailwind CSS.",
  url: "https://tx.shadcn.com",
  ogImage: "https://tx.shadcn.com/og.jpg",
  links: {
    twitter: "https://twitter.com/shadcn",
    github: "https://gitlab.com/abner.oliveira.ce/saas-b2b-boilerplate",
  },
}