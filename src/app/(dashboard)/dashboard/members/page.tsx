
import { EmptyPlaceholder } from "@/components/empty-placeholder";
import { DashboardHeader } from "@/components/header";

import { DashboardShell } from "@/components/shell";

export const metadata = {
  title: "Dashboard",
}

export default async function MemberPage() {

  const data = true;

  return (
    <DashboardShell>
      <DashboardHeader heading="Members Page" text="Only members could access this page.">
       
      </DashboardHeader>
      <div>
        {data ? (
          <div className="divide-y divide-border rounded-md border">
           True
          </div>
        ) : (
          <EmptyPlaceholder>
            <EmptyPlaceholder.Icon name="post" />
            <EmptyPlaceholder.Title>No posts created</EmptyPlaceholder.Title>
            <EmptyPlaceholder.Description>
              You don&apos;t have any posts yet. Start creating content.
            </EmptyPlaceholder.Description>
             Post create button
          </EmptyPlaceholder>
        )}
      </div>
    </DashboardShell>
  )
}
